package com.tyagiabhinav.dialogflowchat.utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Constants {
    public static final String LANG= "LANGUAGES";

    public static String getAllJokes(){
        List<String> jokesList = new ArrayList<>();
        jokesList.add("What did one toilet say to the other?\n" +
                "You look a bit flushed.\n ");
        jokesList.add("What do you think of that new diner on the moon?\n" +
                "Food was good, but there really wasn’t much atmosphere. ");
        jokesList.add("Why did the dinosaur cross the road?\n" +
                "Because the chicken wasn’t born yet. ");
        jokesList.add("Why can’t Elsa from Frozen have a balloon?\n" +
                "Because she will “let it go, let it go.” ");
        jokesList.add("What musical instrument is found in the bathroom?\n" +
                "A tuba toothpaste.\n ");
        jokesList.add("Why did the kid bring a ladder to school?\n" +
                "Because she wanted to go to high school. ");
        jokesList.add("What do you call a dog magician?\n" +
                "A labracadabrador. ");

        Random random =new Random();


        String joke = jokesList.get(random.nextInt(jokesList.size()-1));
        return joke;
    }

    public static String getAllPoems(){
        List<String> poemsList = new ArrayList<>();
        poemsList.add("Jack and Jill\n" +
                "Went up the hill\n" +
                "To fetch a pail of water,\n" +
                "Jack fell down\n" +
                "And broke his crown\n" +
                "And Jill came tumbling after.\n" +
                "Up Jack got\n" +
                "And home did trot\n" +
                "As fast as he could caper,\n" +
                "Went to bed\n" +
                "To mend his head\n" +
                "With vinegar and brown paper.");
        poemsList.add("Two Little Dicky Birds,\n" +
                "Sat upon a wall.\n" +
                "One named Peter,\n" +
                "The other named Paul,\n" +
                "Fly away Peter.\n" +
                "Fly away Paul.\n" +
                "Come back Peter!\n" +
                "Come back Paul!! ");

        Random random =new Random();


        String poem = poemsList.get(random.nextInt(poemsList.size()-1));
        return poem;
    }
}
